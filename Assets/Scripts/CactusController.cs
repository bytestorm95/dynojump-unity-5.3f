﻿using UnityEngine;
using System.Collections;

public class CactusController : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D collider){
		Debug.Log (collider.gameObject.name);
		PlayerController.instance.StopPlaying();
		HUDController.instance.PlayGameOver ();
	}

}
