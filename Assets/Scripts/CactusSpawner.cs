﻿using UnityEngine;
using System.Collections;

public class CactusSpawner : MonoBehaviour {
	public GameObject[] CactusPrefabs;
	float spawnY;
	IEnumerator spawnGenerator;
	// Use this for initialization
	void Start () {
		spawnY = transform.position.y;
		spawnGenerator = SpawnRoutine ();
	}

	void FixedUpdate () {
		spawnGenerator.MoveNext ();
	}

	IEnumerator SpawnRoutine(){
		float timer = 0;
		while (true) {
			timer += Time.fixedDeltaTime;
			if (timer >= 1) {
				timer = 0;
				Vector3 spawnLoc = new Vector3 (GetSpawnX (), spawnY, 0);
				Instantiate (CactusPrefabs [Random.Range(0, CactusPrefabs.Length)], spawnLoc, Quaternion.identity);
			}
			yield return 0;
		}
	}

	float GetSpawnX ()
	{
		return Camera.main.orthographicSize + Camera.main.transform.position.x + 2.0f;
	}
}
