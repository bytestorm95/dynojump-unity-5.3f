﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject targetToFollow;
	float cameraOffset;

	Vector3 position = new Vector3();

	// Use this for initialization
	void Start () {
		position = transform.position;
		float horzExtent = Camera.main.orthographicSize * Screen.width / Screen.height;
		cameraOffset = horzExtent * 0.6f;
	}
	
	// Update is called once per frame
	void Update () {
		position.x = targetToFollow.transform.position.x + cameraOffset;
		transform.position = position;
	}
}
