﻿using UnityEngine;
using System.Collections;

public class GroundDetector : MonoBehaviour {

	public static GroundDetector instance;
	bool isGrounded = false;
	// Use this for initialization
	void Start () {
		instance = this;
	}
	
	public bool IsGrounded(){
		return isGrounded;
	}

	void OnTriggerEnter2D(Collider2D collider){		
		if (collider.gameObject.tag == "Ground" && !collider.isTrigger) {
			isGrounded = true;
		}
	}

	void OnTriggerExit2D(Collider2D collider){
		if (collider.gameObject.tag == "Ground" && !collider.isTrigger) {
			isGrounded = false;
		}
	}
}
