﻿using UnityEngine;
using System.Collections;

public class GroundRepeater : MonoBehaviour {
	public GameObject[] sprites;
	BoxCollider2D groundCollider;
	BoxCollider2D repeatTrigger;

	void Start(){
		foreach (BoxCollider2D bc in gameObject.GetComponents<BoxCollider2D> ()) {
			if (bc.isTrigger) {
				groundCollider = bc;
				break;
			} else {
				repeatTrigger = bc;
			}
		}
	}


	// To prevent overflow of the transform position and also move the platform
	// I can do one thing -> after catching following trigger on the next jump
	// of the player we would move everything back to origin ;) Maybe put a
	// special cactus tree their

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.tag == "Player") {
//			Debug.Log ("Platform Moved");
			groundCollider.offset = groundCollider.offset + new Vector2(20.48f, 0);
			repeatTrigger.offset = repeatTrigger.offset + new Vector2(20.48f, 0);
			foreach (GameObject s in sprites) {
				s.transform.Translate (new Vector3 (20.48f, 0, 0));
			}
		} else {
			//Debug.Log (collider.gameObject.tag);
		}
	}
}
