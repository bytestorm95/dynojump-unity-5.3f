﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDController : MonoBehaviour {

	public static HUDController instance;
	public Text gameoverText;
	public Button gameoverButton;

	Animator gameoverButtonAnim;
	Animator gameoverTextAnim;

	// Use this for initialization
	void Start () {
		instance = this;
		gameoverButtonAnim = gameoverButton.GetComponent<Animator> ();
		gameoverTextAnim = gameoverText.GetComponent<Animator> ();
	}

	public void PlayGameOver(){
		gameoverButtonAnim.SetTrigger ("Gameover");
		gameoverTextAnim.SetTrigger ("Gameover");
	}

	public void RestartGame(){
		Application.LoadLevel (Application.loadedLevel);
	}
}
