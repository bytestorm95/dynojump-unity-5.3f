﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public static PlayerController instance;
	public float speed = 10;
	public Vector2 jumpSpeed;
	public Text ScoreText;

	Vector2 baseJump;
	Rigidbody2D playerRigidbody;
	bool isRunning = false;
	Animator anim;
	AudioSource audioSource;
	bool gameover = false;


	// Use this for initialization
	void Start () {
		playerRigidbody = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		baseJump = jumpSpeed;
		audioSource = GetComponent<AudioSource> ();
		instance = this;
	}

	void Update(){
		if(!gameover){
			ScoreText.text = "DISTANCE\n" + (int)transform.position.x;
		}
	}

	public void StopPlaying(){
		gameover = true;
		anim.SetBool ("IsRunning", false);
	}

	// Update is called once per frame
	void FixedUpdate () {	
		if (!gameover) {	
			if (Input.GetButton ("Jump") || Input.GetMouseButton(0)) {
				if (GroundDetector.instance.IsGrounded ()) {
					if (!audioSource.isPlaying) {
						audioSource.Play ();
					} else {
						if (audioSource.time > 0.5f) {
							audioSource.Stop ();
						}
					}
					playerRigidbody.velocity = new Vector2 (0, 0);
					playerRigidbody.AddForce (jumpSpeed);
				}
			} else {
				if (GroundDetector.instance.IsGrounded ()) {
					playerRigidbody.velocity = new Vector2 (speed, 0);
				}
			}

			if (!isRunning) {
				if (playerRigidbody.velocity.x > 0 && playerRigidbody.velocity.y < 0.01) {
					isRunning = true;
					anim.SetBool ("IsRunning", true);
				}
			} else {
				if (playerRigidbody.velocity.x < 0.01 || playerRigidbody.velocity.y > 0.01) {				
					isRunning = false;
					anim.SetBool ("IsRunning", false);
				}
			}
		}
	}
}
